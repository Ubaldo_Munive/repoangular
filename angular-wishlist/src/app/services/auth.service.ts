import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  login(username: string, password: string):boolean {
    if(username==='user' && password === 'password'){
      localStorage.setItem('username', username);
      return true;
    }
      return false;
  }

  logout(): any {
    return localStorage.getItem('username');
  }

  isLoggedIn(): boolean {
    return this.getUser() !== null;
  }

}
