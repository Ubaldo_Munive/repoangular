import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule, rootEffectsInit } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import Dexie, { Transaction } from 'dexie';
import { TranslateService } from '@ngx-translate/core';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './../app/components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './../app/components/lista-destinos/lista-destinos.component';
/*import { DestinosApiClient } from './models/destinos-api-client.model';*/
import { DestinoDetalleComponent } from './../app/components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './../app/components/form-destino-viaje/form-destino-viaje.component';
import { 
  DestinosViajesState,
  reducerDestinosViajes,
  initializeDestinosViajesState, 
  DestinosViajesEffects
} from './models/destinos-viajes-state.model';
import { from } from 'rxjs';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { async } from 'rxjs/internal/scheduler/async';
import { destinoviaje } from './models/destino-viaje.model';
import { LockChanges } from '@ngrx/store-devtools/src/actions';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

//app config
exports interface AppConfig {
  apiEndpoint: string;
};
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// fin app config

export const childrenRoutesVuelos: Routes = [
  {path: '', redirectTo: 'main', pathMatch:'full'},
  {path: 'main', component: VuelosMainComponent},
  {path: 'mas-info', component: VuelosMasInfoComponent},
  {path: ':id', component: VuelosDetalleComponent},
];


const routes: Routes =[
  { path: '', redirectTo:'home', pathMatch:'full'},
  { path: 'home', component: ListaDestinosComponent},
  { path: 'destino/:id', component: DestinoDetalleComponent},
  { path: 'login', component: LoginComponent},
  {
    path: 'protected', 
    component: ProtectedComponent,
    canActivate: [ UsuarioLogueadoGuard]
  },
  {
    path: 'vuelos',
    component: VuelosComponent,
    canActivate:[ UsuarioLogueadoGuard],
    children: childrenRoutesVuelos
  }
];


// redux init
export interface AppState {
  destinos: DestinosViajesState;
}
const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

let reducersInitialState = {
  destinos: initializeDestinosViajesState
}

// redux fin init

// app init
export function init_app(apploadService: AppLoadService): () => Promise<any> {
  return () => apploadService.initializeDestinosViajesState();
}

@Injectable()
class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient){}
    async initializeDestinosViajesState(): Promise<any> {
      const header: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
      const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + 'my', {headers: Headers});
      const response: any = await this.http.request(req).toPromise();
      this.store.dispatch(new IniMyDataAction(response.body));
    }
  }


//Fin app init

//dixie db

export class translation {
constructor (public id: number, public lang: string, public key: string, public value: string){}
}

@Injectable({
      providedIn: 'root'
    })
}
export class MyDatabase extends Dexie{
  destinos: Dexie.Table<destinoviaje, number>;
  translation: Dexie.Table<Transaction, number>;
  translations: any;
  constructor(){
    super('MyDatabase');
    this.version(1).stores({
      destinos: '++id, nombre, imageUrl'
    });
    this.version(2).stores({
      destinos: '++id, nombre, imageUrl',
      translation: '++id, lang, key, value'
    });
  }
}

export const db = new MyDatabase();
//fin dixie db

//i18n ini
class TranslationLoader implements TranslationLoader{
  constructor(private http: HttpClient){}

  getTranslation(lang: string) Observable<any>{
    const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(result => {
                          if( result.length===0){
                            return this.http
                            .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang')
                            .toPromise()
                            .then(apiResult => {
                              db.translation.bulkAdd(apiResults);
                              return apiResults;
                            });
                          }
                          return results;
                      }).then((traducciones) => {
                        console.log('traducciones cargadas');
                        console.log('traducciones');
                        return traducciones;
                      }).then((traducciones)=> {
                        return traducciones.map((t)=> ({[t.key]: t.value}));
                      });
    return from(promise).pipe(flatMap((elems)=> from(elems)));
  }
}
function HttpLoaderFactory(http: HttpClient){
  return new TranslationLoader(http);
}
//fin i18n ini

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponent,
    EspiameDirective,
    TrackearClickDirective
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState}),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    BrowserAnimationModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory : (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    
  ],
  providers: [
    /*DestinosApiClient,*/
    AuthService, UsuarioLogueadoGuard,
    { provide: APP_CONFIG, userValue: APP_CONFIG_VALUE},
    AppLoadService, 
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true},
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
