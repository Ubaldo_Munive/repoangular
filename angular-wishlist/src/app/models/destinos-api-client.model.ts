import { destinoviaje } from './destino-viaje.model';
import { Subject, BehaviorSubject, from } from 'rxjs';
import { AppState, APP_CONFIG } from '../app.module';
import { Store } from '@ngrx/store';
import { 
  NuevoDestinoAction, 
  ElegidoFavoritoAction } from './destinos-viajes-state.model';
import { Injectable, inject, forwardRef } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders, HttpResponse, HttpRequest } from '@angular/common/http';

@Injectable()
export class DestinosApiClient {
  destinos: destinoviaje[]=[];

  constructor(
    private store: Store<AppState>,
    @inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
    private http: HttpClient
    ) {
      
    this.store
    .select(state => state.destinos)
    .subscribe((data)=> {
      console.log('destinos sub store');
      console.log(data);
      this.destinos = data.items;
    });
    this.store
    .subscribe((data)=> {
    console.log('all store');
    console.log(data);
    });
  }

  add(d: destinoviaje){
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
    const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', {nuevo: d.nombre}, {headers: Headers});
    this.http.request(req).subscribe((data: HttpResponse<{}>)=> {
      if (data.status === 200){
         this.store.dispatch(new NuevoDestinoAction(d));
         const myDb = db;
         myDb.destinos.add(d);
         console.log('todos los destinos de la db|');
         myDb.destinos.toArray().then(destinos => console.log(destinos))
      }
    });
  }
  
  getById(id: string): destinoviaje {
    return this.destinos.filter(function(d) {return d.id.tostring()=== id; }) [0];
  }
  
  getAll():destinoviaje[]{
    return this.destinos;
  }

  elegir(d: destinoviaje){
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}