import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {destinoviaje} from '../../models/destino-viaje.model';
import {DestinosApiClient} from '../../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { STATE_PROVIDERS, State } from '@ngrx/store/src/state';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destino/lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.less'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<destinoviaje>;
  updates: string[];
  all;

  constructor(private DestinosApiClient:DestinosApiClient, private store: Store<AppState>) { 
  this.onItemAdded=new EventEmitter();
  this.updates = [];
  this.store.select(state => state.destinos.favorito)
  .subscribe(d => {
  })
  this.DestinosApiClient.subscribeOnChange((d: destinoviaje) => {
    if (d != null){
      this.updates.push('se ha elegido a ' + d.nombre);
    }
  })
  store.select(State => State.destinos.items).subscribe(items => this.all = items)
  }
  ngOnInit(): void {
  }

  agregado(d: destinoviaje){
    this.DestinosApiClient.add(d);
    this.onItemAdded.emit(d);
        
  }

  elegido(e: destinoviaje){
    this.DestinosApiClient.elegir(e);
        
  }
getAll(){}
}

