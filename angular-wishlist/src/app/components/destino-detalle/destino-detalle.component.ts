import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { destinoviaje } from '../../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { AppState } from 'src/app/app.module';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.less'],
  providers: [ DestinosApiClient],
})

export class DestinoDetalleComponent implements OnInit{
  destinos: destinoviaje;
  style={
    sources:{
      world: {
        type : 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.jason/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      'id':,'countries',
      'type': 'fill',
      'sources':'world',
      'paint':{
        'fill-color': '#6F788A'
      }
    }]
  };

  constructor(private route:ActivatedRoute, private destinoApiClient: DestinosApiClient){}
ngOnInit(){
  const id=this.route.snapshot.paramMap.get('id');
  this.destino= this.destinoApiClient,getByid('Id');
}

}