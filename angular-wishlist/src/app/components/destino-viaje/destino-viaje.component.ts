import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { destinoviaje } from '../../models/destino-viaje.model';
import { VoteUpAction, VoteDownAction } from '../../models/destinos-viajes-state.model';
import { trigger, state, style, transition, animate } from '@angular/animations';


@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.less'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
       })),
       state('estadoNofavorito', style({
        backgroundColor: 'WhiteSmoke' 
       })),
       transition('estadoNoFavorito => estadoFavorito', [
         animate('3s')
       ]),
       transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
    ]
})
export class DestinoViajeComponent implements OnInit {
  @Input() destino: destinoviaje;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass='col-md-4';
  @Output() clicked: EventEmitter<destinoviaje>;
  store: any;

  constructor() {
    this.clicked=new EventEmitter();

  }
  ir(){
    this.clicked.emit(this.destino);
    return false;

  }

  ngOnInit(): void {
  }

voteUp(){
this.store.dispatch(new VoteUpAction(this.destino));
return;
}
voteDown(){
  this.store.dispatch(new VoteDownAction(this.destino));
return false;
}
}
